CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module tracks, by Ajax, how long a registered user took to read a page, and
the time spent on the entire site.

You can configure which content type and role should be tracked and view the
report, exhibiting the total hours (or minutes/seconds) a user spent reading
a page. The interval between one Ajax call and other can be configurable
trough UI as well.

REQUIREMENTS
------------

No other modules are required.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. For further
information, see:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules

CONFIGURATION
-------------

Visit /admin/config/people/time_spent for the configuration.


USE CASE
--------

- You need to track and exhibit a statistic report with the amount of time
a registered user took reading a node page, and the entire time he spent on your
site.
- Module creates the path - admin/reports/time_spent/timespent-list-users
which lists all the users along with the total time spent by them in
a tabular format.
- Path admin/reports/time_spent/timespent-list-users has autocomplete user
search text field which helps you in getting the complete report of that user.


LIMITATIONS
-----------

- This module only tracks node pages and registered users. Taxonomy or views
pages aren't tracked, but can be in later versions.
- If you need to track anonymous visits, consider using Google Analytics
(GA can't track registered users by its privacy terms).
- If you have cache mode on, you need to use hook_boot instead. Change for
time_spent_boot where it is time_spent_init.
- At this moment, people can fool the system if the user has Java Script
knowledge and access directly the url time_spent/1 and hit refresh a couple
of times. The system will think the user was on node/1 for a couple of minutes.


MAINTAINERS
-----------

Current maintainers:
  * Håvard Pedersen (fuzzy76) - https://www.drupal.org/u/fuzzy76
  * Farklı Bakış Açıları (carvalhar) - https://www.drupal.org/user/366309
  * Jonathan Hedstrom (jhedstrom) - https://www.drupal.org/u/jhedstrom
  * Neetu Morwani (neetu morwani) - https://www.drupal.org/u/neetu-morwani
  * Piyuesh Kumar (piyuesh23) - https://www.drupal.org/u/piyuesh23
  * Purushotam Rai (purushotam.rai) - https://www.drupal.org/u/purushotamrai
  * Rahul Shinde (rahul.shinde) - https://www.drupal.org/u/rahulshinde
