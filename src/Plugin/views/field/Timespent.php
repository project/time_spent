<?php

namespace Drupal\time_spent\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Form\FormStateInterface;

/**
 * A handler to provide proper displays for time spent.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("time_spent")
 */
class Timespent extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['time_spent']['contains']['granularity'] = ['default' => 5];
    $options['time_spent']['contains']['time_spent_type'] = ['default' => 'time_spent_sec2hms'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['time_spent'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Time Spent'),
    ];

    $form['time_spent']['time_spent_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display type'),
      '#options' => [
        'time_spent_sec2hms' => $this->t('Sec to HMS (Hours Minutes Seconds)'),
        'granularity' => $this->t('Granularity'),
      ],
      '#default_value' => $this->options['time_spent']['time_spent_type'],
    ];

    $form['time_spent']['granularity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Granularity'),
      '#description' => $this->t('How many different units to display in the string.'),
      '#states' => [
        'visible' => [
          ':input[name="options[time_spent][time_spent_type]"]' => ['value' => 'granularity'],
        ],
      ],
      '#default_value' => $this->options['time_spent']['granularity'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);

    if ($this->options['time_spent']['time_spent_type'] == 'granularity') {
      $granularity = isset($this->options['time_spent']['granularity']) ? $this->options['time_spent']['granularity'] : 5;
      $time_spent = \Drupal::service('date.formatter')->formatInterval($value, $granularity);
    }
    else {
      $time_spent = time_spent_sec2hms($value);
    }
    return $time_spent;
  }

}
