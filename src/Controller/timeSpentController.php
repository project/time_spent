<?php

namespace Drupal\time_spent\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for user routes.
 */
class TimeSpentController extends ControllerBase {

  /**
   * The default database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The time_spent settings config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $timeSpentConfig;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a Time spent controller.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The Connection object containing the key-value tables.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configuration
   *   The system file configuration.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(
    Connection $database,
    ConfigFactoryInterface $configuration,
    PagerManagerInterface $pager_manager,
    UserStorageInterface $user_storage,
    RendererInterface $renderer,
    FormBuilderInterface $form_builder
  ) {
    $this->database = $database;
    $this->timeSpentConfig = $configuration->get('time_spent.settings');
    $this->pagerManager = $pager_manager;
    $this->userStorage = $user_storage;
    $this->renderer = $renderer;

    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('pager.manager'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('renderer'),
      $container->get('form_builder')
    );
  }

  /**
   * Reporting time spent by a user on page.
   *
   * @throws \Exception
   */
  public function timeSpentReports() {
    $header = [
      $this->t('Post'),
      $this->t('User'),
      $this->t('Time Spent'),
    ];
    $rows = [];
    $output = [];
    $pager = $this->timeSpentConfig->get('time_spent_pager_limit');

    $query = $this->database->select('time_spent_page', 'tsp');
    $query->fields('tsp', ['timespent']);
    $query->fields('u', ['uid']);
    $query->fields('node', ['nid', 'title']);
    $query->join('node_field_data', 'node', 'node.nid = tsp.nid');
    $query->join('users_field_data', 'u', 'u.uid = tsp.uid');

    $count_result = $query->execute();
    $count_result->allowRowCount = TRUE;
    $number = $count_result->rowCount();

    $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $query->limit($pager);
    $query->addTag('node_access');

    $nodes = $query->execute()->fetchAllAssoc('nid');
    foreach ($nodes as $row) {
      $user = $this->userStorage->load($row->uid);
      $username = [
        '#theme' => 'username',
        '#account' => $user,
      ];
      $username = $this->renderer->render($username);

      $url = Url::fromRoute('entity.node.canonical', ['node' => $row->nid]);
      $rows[] = [
        Link::fromTextAndUrl($row->title, $url),
        $username,
        time_spent_sec2hms($row->timespent),
      ];
    }

    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'style' => 'width:630px',
      ],
      '#prefix' => $this->t("<h3>Time spent on each node page by users</h3>"),
    ];

    $output[] = $table;
    $this->pagerManager->createPager($number, $pager);

    if ($number > $pager) {
      $pager_output = ['#theme' => 'pager'];
      $output[] = $pager_output;
    }

    $header = [
      $this->t('User'),
      $this->t('Time Spent'),
    ];
    $rows = [];
    $query = $this->database->select('time_spent_site', 'tss');
    $query->fields('tss', ['timespent']);
    $query->fields('u', ['uid', 'name']);
    $query->join('users_field_data', 'u', 'u.uid = tss.uid');

    $count_result = $query->execute();
    $count_result->allowRowCount = TRUE;
    $number = $count_result->rowCount();

    $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $query->limit($pager);
    $query->addTag('node_access');

    $nodes = $query->execute()->fetchAllAssoc('name');
    $number = count($nodes);

    foreach ($nodes as $row) {
      $user = $this->userStorage->load($row->uid);
      $username = [
        '#theme' => 'username',
        '#account' => $user,
      ];
      $username = $this->renderer->render($username);
      $rows[] = [
        $username,
        time_spent_sec2hms($row->timespent),
      ];
    }

    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'style' => 'width:630px',
      ],
      '#prefix' => $this->t("<h3>Time spent on the entire site by each user</h3>"),
    ];

    $this->pagerManager->createPager($number, $pager);
    $output[] = $table;

    if ($number > $pager) {
      $pager = ['#theme' => 'pager'];
      $output[] = $pager;
    }

    $header = [$this->t('Post'), $this->t('User'), $this->t('Time Spent')];
    $rows = [];
    $query = $this->database->select('time_spent_page', 'tsu');
    $query->fields('tsu', ['timespent']);
    $query->fields('u', ['uid']);
    $query->fields('node', ['nid', 'title']);
    $query->join('node_field_data', 'node', 'node.nid = tsu.nid');
    $query->join('users_field_data', 'u', 'u.uid = tsu.uid');
    $query->orderBy('node.nid', 'DESC');

    $count_result = $query->execute();
    $count_result->allowRowCount = TRUE;
    $number = $count_result->rowCount();

    $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $query->limit($pager);
    $query->addTag('node_access');

    $nodes = $query->execute()->fetchAll();
    foreach ($nodes as $row) {
      $user = $this->userStorage->load($row->uid);
      $username = [
        '#theme' => 'username',
        '#account' => $user,
      ];
      $username = $this->renderer->render($username);
      $url = Url::fromRoute('entity.node.canonical', ['node' => $row->nid]);
      $rows[] = [
        Link::fromTextAndUrl($row->title, $url),
        $username,
        time_spent_sec2hms($row->timespent),
      ];
    }

    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'style' => 'width:630px',
      ],
      '#prefix' => $this->t("<h3>Time spent by user on individual page</h3>"),
    ];

    $output[] = $table;
    $this->pagerManager->createPager($number, $pager);

    if ($number > $pager) {
      $pager = ['#theme' => 'pager'];
      $output[] = $pager;
    }

    return $output;
  }

  /**
   * Time Spent Listing.
   */
  public function timespentListUsers() {

    $output = [];
    $output[] = $this->formBuilder->getForm('Drupal\time_spent\Form\TimeSpentUserReportForm');
    $pager = $this->timeSpentConfig->get('time_spent_pager_limit');
    $header = [
      $this->t('User'),
      $this->t('Time Spent'),
    ];
    $rows = [];
    $query = $this->database->select('time_spent_site', 'tss');
    $query->fields('tss', ['timespent']);
    $query->fields('u', ['uid', 'name']);
    $query->join('users_field_data', 'u', 'u.uid = tss.uid');

    $count_result = $query->execute();
    $count_result->allowRowCount = TRUE;
    $number = $count_result->rowCount();

    $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $query->limit($pager);
    $query->addTag('user_access');

    $nodes = $query->execute()->fetchAllAssoc('name');

    foreach ($nodes as $row) {
      $user = $this->userStorage->load($row->uid);
      $url = Url::fromRoute('time_spent.user_report', ['user' => $user->id()]);
      $rows[] = [
        Link::fromTextAndUrl($row->name, $url),
        time_spent_sec2hms($row->timespent),
      ];
    }

    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'style' => 'width:600px',
      ],
      '#prefix' => $this->t("<h3>Time spent on the entire site by each user</h3>"),
    ];

    $this->pagerManager->createPager($number, $pager);
    $output[] = $table;

    if ($number > $pager) {
      $pager = ['#theme' => 'pager'];
      $output[] = $pager;
    }

    return $output;
  }

  /**
   * Time spent per user on each node.
   *
   * @throws \Exception
   */
  public function timespentUserDetail(UserInterface $user) {
    $userid = $user->id();
    $header = [
      $this->t('Post'),
      $this->t('User'),
      $this->t('Time Spent'),
    ];
    $rows = [];
    $output = [];
    $pager = $this->timeSpentConfig->get('time_spent_pager_limit');

    $query = $this->database->select('time_spent_page', 'tsp');
    $query->fields('tsp', ['timespent']);
    $query->fields('u', ['uid']);
    $query->fields('node', ['nid', 'title']);
    $query->condition('u.uid', $userid, '=');
    $query->join('node_field_data', 'node', 'node.nid = tsp.nid');
    $query->join('users_field_data', 'u', 'u.uid = tsp.uid');

    $count_result = $query->execute();
    $count_result->allowRowCount = TRUE;
    $number = $count_result->rowCount();

    $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $query->limit($pager);
    $query->addTag('user_access');

    $nodes = $query->execute()->fetchAllAssoc('nid');

    $username = [
      '#theme' => 'username',
      '#account' => $user,
    ];
    $username = $this->renderer->render($username);

    foreach ($nodes as $row) {
      $url = Url::fromRoute('entity.node.canonical', ['node' => $row->nid]);
      $rows[] = [
        Link::fromTextAndUrl($row->title, $url),
        $username,
        time_spent_sec2hms($row->timespent),
      ];
    }

    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'style' => 'width:600px',
      ],
      '#prefix' => $this->t("<h3>Time spent on each node page by user</h3>"),
    ];

    $this->pagerManager->createPager($number, $pager);
    $output[] = $table;

    if ($number > $pager) {
      $pager = ['#theme' => 'pager'];
      $output[] = $pager;
    }

    return $output;
  }

  /**
   * Saving time spent on a node.
   */
  public function timespentAjaxCheck($nid) {
    $user = $this->currentUser();
    $timer = (int) $this->timeSpentConfig->get('time_spent_timer');

    // Monitors the time user spent on the site.
    $query = $this->database->query("SELECT timespent FROM {time_spent_site} WHERE uid =" . $user->id());
    $timespent = $query->fetchField();

    if ($timespent) {
      // Update the record, increments each time.
      $result = $this->database->query("UPDATE {time_spent_site} SET timespent = :timespent  WHERE uid = :uid",
        [':timespent' => ($timespent + $timer), ':uid' => $user->id()]);
    }
    else {
      // Record the first time for this user.
      $this->database->insert('time_spent_site')
        ->fields([
          'uid' => (int) $user->id(),
          'timespent' => $timer,
        ])
        ->execute();
    }
    unset($timespent);
    unset($result);
    // Monitors time spent by page.
    if (is_numeric($nid) && $nid > 0) {
      // Firstly detect if this node already has a record for this user.
      $sql = "SELECT timespent FROM {time_spent_page} WHERE nid = :nid AND uid = :uid";
      $timespent = $this->database->query($sql, [':nid' => $nid, ':uid' => $user->id()])->fetchField();

      if ($timespent) {
        // Update the record, increments each time.
        $result = $this->database->query(
          "UPDATE {time_spent_page} SET timespent= :timespent WHERE nid = :nid AND uid = :uid",
          [
            ':timespent' => ($timespent + $timer),
            ':nid' => $nid,
            ':uid' => $user->id(),
          ]
        );

        return [
          '#markup' => $this->t('Record updated successfully!'),
        ];
      }
      else {
        // Record the first time for this user at this node page.
        $this->database->insert('time_spent_page')
          ->fields([
            'uid' => (int) $user->id(),
            'timespent' => $timer,
            'nid' => $nid,
          ])
          ->execute();

        return [
          '#markup' => $this->t('Record inserted successfully!'),
        ];
      }
    }
  }

}
