<?php

/**
 * @file
 * Views plugin settings in this file.
 */

/**
 * Implements hook_views_data().
 */
function time_spent_views_data() {
  $data = [];

  // Create a new group by which the of this module plugins would be grouped.
  $data['time_spent_page']['table']['group'] = t('Time Spent');

  $data['time_spent_page']['table']['provider'] = 'time_spent';

  $data['time_spent_page']['table']['base'] = [
    'field' => 'time_spent_id',
    'title' => t('Time spent'),
    'defaults' => [
      'field' => 'time_spent_id',
    ],
  ];

  $data['time_spent_page']['table']['join'] = [
    'node' => [
      'left_field' => 'nid',
      'field' => 'nid',
    ],
  ];

  $data['time_spent_page']['uid'] = [
    'title' => t('User ID'),
    'help' => t('Time spent user ID.'),
    'relationship' => [
      'base' => 'users',
      'base field' => 'uid',
      'id' => 'standard',
      'label' => t('Timespent user'),
    ],
  ];

  $data['time_spent_page']['time_spent_id'] = [
    'title' => t('Time spent id'),
    'help' => t('Unique id representing a time_spent entry'),
    'field' => [
      'id' => 'standard',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $data['time_spent_page']['timespent'] = [
    // The item it appears as on the UI.
    'title' => t('Time spent length'),
    'help' => t('The timespent length'),
    'field' => [
      'id' => 'time_spent',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'id' => 'date',
    ],
  ];

  return $data;
}
